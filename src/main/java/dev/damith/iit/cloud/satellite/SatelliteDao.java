package dev.damith.iit.cloud.satellite;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface SatelliteDao extends ReactiveMongoRepository<Satellite,Long> {
}
