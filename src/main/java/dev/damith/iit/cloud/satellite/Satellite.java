package dev.damith.iit.cloud.satellite;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Satellite {
    private String name;
    private Double Latitude; // 0 - 90
    private Double Longitude; // 0 -180
    private Integer health; // 0 - 100
    private Boolean active;
}
