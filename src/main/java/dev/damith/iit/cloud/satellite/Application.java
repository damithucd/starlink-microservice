package dev.damith.iit.cloud.satellite;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	ApplicationRunner init(SatelliteDao repository){
		Satellite[] data = {
				new Satellite("Starlink-1",21.55,33.46,95,true),
				new Satellite("Starlink-2",55.23,98.05,95,true),
				new Satellite("Starlink-3",26.94,157.95,95,true),
		};

		return args -> {
			repository
					.deleteAll()
					.thenMany(
							Flux
									.just(data)
									.map(item -> {
										return item;
									})
									.flatMap(repository::save)
					)
					.thenMany(repository.findAll())
					.subscribe(kayak -> System.out.println("saving " + kayak.toString()));

		};
	}
}
