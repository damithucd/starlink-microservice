package dev.damith.iit.cloud.satellite;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@RequestMapping(path = "/constellation")
public class SatelliteController {

    @Autowired
    SatelliteDao satelliteRepo;

    @PostMapping()
    @ResponseBody
    Mono<Satellite> addSatellite(@RequestBody Satellite satellite){
        return satelliteRepo.save(satellite);
    }



    @GetMapping
    @ResponseBody
    Flux<Satellite> getAllSatellites(){
        return satelliteRepo.findAll();
    }

}
